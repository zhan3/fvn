#!/bin/env python

import networkx as nx
import scipy as sp
import numpy as np
import pandas as pd
from scipy import sparse
import sys, getopt

################################################
def main(argv):
	
	'''
		Read in gml file and turn into csr matrix components: indices, indptr
		assumpe in gml the node is denoted by consecutive numbers from 0 to N-1
	'''

	# Read in file name
	opts, args = getopt.getopt(argv,"F:",["File="])
	
	file_name = None
	for opt, arg in opts:
	       	if opt in ("-F","--File"):
			file_name = arg
	
	if not file_name:
		print "File Name needed"
		return 0 

	#Read in data and visualize
	gml_data = nx.read_gml('./raw_data/%s' %file_name)

	#make some corrections
	#footballData.node[110]['value'] = 11
	#footballData.node[50]['value'] = 5
	#footballData.node[59]['value'] = 5
	#footballData.node[58]['value'] = 5
	#footballData.node[90]['value'] = 12
	#footballData.node[28]['value'] = 12

	#number of nodes
	NumNodes = len(gml_data.node)
	print 'total nodes in the dataset %d' %NumNodes

	#number of Edges
	NumEdges = len(gml_data.edges())
	print 'total edges in the dataset %d' %NumEdges

	#Read in adacency matrix and visualize
	Y = np.zeros((NumNodes,NumNodes),dtype = bool)
	for edge in gml_data.edges():
    		Y[edge[0],edge[1]] = Y[edge[1],edge[0]] = 1
	Contingency_csr = sparse.csr_matrix(Y)

	#store infor to disk
	col = Contingency_csr.indices
	row = Contingency_csr.indptr
	print 'Size of the output \n'
	print '%sMB' %(col.nbytes/1000000.)
	print '%sMB' %(row.nbytes/1000000.)

	#Port to C++ for cuda programming
	row.tofile('./data/row.txt','\n')
	col.tofile('./data/col.txt','\n')

	print 'Completed'
