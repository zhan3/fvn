graph [
  node [
    id 0
    label "BrighamYoung"
    longitude -111.6493177
    value 7
    latitude 40.2518429
  ]
  node [
    id 1
    label "FloridaState"
    longitude -84.2906658
    value 0
    latitude 30.4409616
  ]
  node [
    id 2
    label "Iowa"
    longitude -91.5548998
    value 2
    latitude 41.6626963
  ]
  node [
    id 3
    label "KansasState"
    longitude -96.5847249
    value 3
    latitude 39.1974437
  ]
  node [
    id 4
    label "NewMexico"
    longitude -106.6197553
    value 7
    latitude 35.0843441
  ]
  node [
    id 5
    label "TexasTech"
    longitude -101.8782822
    value 3
    latitude 33.5842591
  ]
  node [
    id 6
    label "PennState"
    longitude -77.8599084
    value 2
    latitude 40.7982133
  ]
  node [
    id 7
    label "SouthernCalifornia"
    longitude -118.2851232
    value 8
    latitude 34.0223383
  ]
  node [
    id 8
    label "ArizonaState"
    longitude -112.0730317
    value 8
    latitude 33.4535269
  ]
  node [
    id 9
    label "SanDiegoState"
    longitude -117.0713197
    value 7
    latitude 32.7746247
  ]
  node [
    id 10
    label "Baylor"
    longitude -97.1184377
    value 3
    latitude 31.5482204
  ]
  node [
    id 11
    label "NorthTexas"
    longitude -97.1525862
    value 10
    latitude 33.207488
  ]
  node [
    id 12
    label "NorthernIllinois"
    longitude -88.773718
    value 6
    latitude 41.934299
  ]
  node [
    id 13
    label "Northwestern"
    longitude -87.6748758
    value 2
    latitude 42.0539102
  ]
  node [
    id 14
    label "WesternMichigan"
    longitude -85.6152186
    value 6
    latitude 42.2831778
  ]
  node [
    id 15
    label "Wisconsin"
    longitude -89.4124875
    value 2
    latitude 43.076592
  ]
  node [
    id 16
    label "Wyoming"
    longitude -105.5796706
    value 7
    latitude 41.3130972
  ]
  node [
    id 17
    label "Auburn"
    longitude -85.4951663
    value 9
    latitude 32.5933574
  ]
  node [
    id 18
    label "Akron"
    longitude -81.5102535
    value 6
    latitude 41.0763656
  ]
  node [
    id 19
    label "VirginiaTech"
    longitude -80.4234167
    value 1
    latitude 37.2283843
  ]
  node [
    id 20
    label "Alabama"
    longitude -87.5474349
    value 9
    latitude 33.2144445
  ]
  node [
    id 21
    label "UCLA"
    longitude -118.4451812
    value 8
    latitude 34.068921
  ]
  node [
    id 22
    label "Arizona"
    longitude -112.0662126
    value 8
    latitude 33.4522217
  ]
  node [
    id 23
    label "Utah"
    longitude -111.8421021
    value 7
    latitude 40.7649368
  ]
  node [
    id 24
    label "ArkansasState"
    longitude -90.6766749
    value 10
    latitude 35.8383947
  ]
  node [
    id 25
    label "NorthCarolinaState"
    longitude -78.6820946
    value 0
    latitude 35.7846633
  ]
  node [
    id 26
    label "BallState"
    longitude -85.4088412
    value 6
    latitude 40.1972366
  ]
  node [
    id 27
    label "Florida"
    longitude -82.346394
    value 9
    latitude 29.645264
  ]
  node [
    id 28
    label "BoiseState"
    longitude -116.2087065
    value 11
    latitude 43.6036004
  ]
  node [
    id 29
    label "BostonCollege"
    longitude -71.1684945
    value 1
    latitude 42.3355488
  ]
  node [
    id 30
    label "WestVirginia"
    longitude -79.9559358
    value 1
    latitude 39.6361396
  ]
  node [
    id 31
    label "BowlingGreenState"
    longitude -83.6300826
    value 6
    latitude 41.3797788
  ]
  node [
    id 32
    label "Michigan"
    longitude -83.7382259
    value 2
    latitude 42.2780584
  ]
  node [
    id 33
    label "Virginia"
    longitude -78.5079772
    value 0
    latitude 38.0335529
  ]
  node [
    id 34
    label "Buffalo"
    longitude -78.8170273
    value 6
    latitude 42.953672
  ]
  node [
    id 35
    label "Syracuse"
    longitude -76.1351158
    value 1
    latitude 43.0391534
  ]
  node [
    id 36
    label "CentralFlorida"
    longitude -81.2000599
    value 5
    latitude 28.6024276
  ]
  node [
    id 37
    label "GeorgiaTech"
    longitude -84.396285
    value 0
    latitude 33.7756178
  ]
  node [
    id 38
    label "CentralMichigan"
    longitude -84.7751384
    value 6
    latitude 43.5912088
  ]
  node [
    id 39
    label "Purdue"
    longitude -86.9211938
    value 2
    latitude 40.4237096
  ]
  node [
    id 40
    label "Colorado"
    longitude -105.2659419
    value 3
    latitude 40.0075811
  ]
  node [
    id 41
    label "ColoradoState"
    longitude -105.0865473
    value 7
    latitude 40.573436
  ]
  node [
    id 42
    label "Connecticut"
    longitude -72.2539805
    value 5
    latitude 41.8077414
  ]
  node [
    id 43
    label "EasternMichigan"
    longitude -83.6246896
    value 6
    latitude 42.2505512
  ]
  node [
    id 44
    label "EastCarolina"
    longitude -77.366959
    value 4
    latitude 35.607482
  ]
  node [
    id 45
    label "Duke"
    longitude -78.9423439
    value 0
    latitude 35.9971342
  ]
  node [
    id 46
    label "FresnoState"
    longitude -119.7496487
    value 11
    latitude 36.8117049
  ]
  node [
    id 47
    label "OhioState"
    longitude -83.0310846
    value 2
    latitude 40.0138423
  ]
  node [
    id 48
    label "Houston"
    longitude -95.3422334
    value 4
    latitude 29.7199489
  ]
  node [
    id 49
    label "Rice"
    longitude -95.4018312
    value 11
    latitude 29.7173941
  ]
  node [
    id 50
    label "Idaho"
    longitude -117.0126084
    value 10
    latitude 46.7288124
  ]
  node [
    id 51
    label "Washington"
    longitude -122.3035191
    value 8
    latitude 47.6553354
  ]
  node [
    id 52
    label "Kansas"
    longitude -95.251663
    value 3
    latitude 38.9544238
  ]
  node [
    id 53
    label "SouthernMethodist"
    longitude -96.7845175
    value 11
    latitude 32.8412178
  ]
  node [
    id 54
    label "Kent"
    longitude -81.3414649
    value 6
    latitude 41.1490629
  ]
  node [
    id 55
    label "Pittsburgh"
    longitude -79.960835
    value 1
    latitude 40.4443533
  ]
  node [
    id 56
    label "Kentucky"
    longitude -84.5039697
    value 9
    latitude 38.0306511
  ]
  node [
    id 57
    label "Louisville"
    longitude -85.7506403
    value 4
    latitude 38.227287
  ]
  node [
    id 58
    label "LouisianaTech"
    longitude -92.6499581
    value 11
    latitude 32.5283255
  ]
  node [
    id 59
    label "LouisianaMonroe"
    longitude -92.0724303
    value 10
    latitude 32.5287009
  ]
  node [
    id 60
    label "Minnesota"
    longitude -93.2277285
    value 2
    latitude 44.97399
  ]
  node [
    id 61
    label "MiamiOhio"
    longitude -84.7308768
    value 6
    latitude 39.5105334
  ]
  node [
    id 62
    label "Vanderbilt"
    longitude -86.8027437
    value 9
    latitude 36.1443747
  ]
  node [
    id 63
    label "MiddleTennesseeState"
    longitude -86.3593732
    value 10
    latitude 35.8467089
  ]
  node [
    id 64
    label "Illinois"
    longitude -88.2271615
    value 2
    latitude 40.1019523
  ]
  node [
    id 65
    label "MississippiState"
    longitude -88.7901613
    value 9
    latitude 33.4548567
  ]
  node [
    id 66
    label "Memphis"
    longitude -89.9371249
    value 4
    latitude 35.1189522
  ]
  node [
    id 67
    label "Nevada"
    longitude -119.8167317
    value 11
    latitude 39.5445866
  ]
  node [
    id 68
    label "Oregon"
    longitude -123.0726062
    value 8
    latitude 44.0448303
  ]
  node [
    id 69
    label "NewMexicoState"
    longitude -106.7541649
    value 10
    latitude 32.2822923
  ]
  node [
    id 70
    label "SouthCarolina"
    longitude -81.0274276
    value 9
    latitude 33.996112
  ]
  node [
    id 71
    label "Ohio"
    longitude -82.1014242
    value 6
    latitude 39.3243417
  ]
  node [
    id 72
    label "IowaState"
    longitude -93.6464654
    value 3
    latitude 42.0266187
  ]
  node [
    id 73
    label "SanJoseState"
    longitude -121.8810715
    value 11
    latitude 37.3351874
  ]
  node [
    id 74
    label "Nebraska"
    longitude -96.7004763
    value 3
    latitude 40.8201966
  ]
  node [
    id 75
    label "SouthernMississippi"
    longitude -89.3357393
    value 4
    latitude 31.3310077
  ]
  node [
    id 76
    label "Tennessee"
    longitude -83.9294564
    value 9
    latitude 35.9544013
  ]
  node [
    id 77
    label "Stanford"
    longitude -122.169719
    value 8
    latitude 37.4274745
  ]
  node [
    id 78
    label "WashingtonState"
    longitude -117.154422
    value 8
    latitude 46.7319374
  ]
  node [
    id 79
    label "Temple"
    longitude -75.1546786
    value 1
    latitude 39.9820942
  ]
  node [
    id 80
    label "Navy"
    longitude -76.4859689
    value 5
    latitude 38.9818524
  ]
  node [
    id 81
    label "TexasA&M"
    longitude -96.3408696
    value 3
    latitude 30.6103946
  ]
  node [
    id 82
    label "NotreDame"
    longitude -86.2353067
    value 5
    latitude 41.7052193
  ]
  node [
    id 83
    label "TexasElPaso"
    longitude -106.506257
    value 11
    latitude 31.7741317
  ]
  node [
    id 84
    label "Oklahoma"
    longitude -97.4457137
    value 3
    latitude 35.2058936
  ]
  node [
    id 85
    label "Toledo"
    longitude -83.6126979
    value 6
    latitude 41.6621416
  ]
  node [
    id 86
    label "Tulane"
    longitude -90.1207279
    value 4
    latitude 29.9403477
  ]
  node [
    id 87
    label "Mississippi"
    longitude -89.5384565
    value 9
    latitude 34.3647402
  ]
  node [
    id 88
    label "Tulsa"
    longitude -95.943613
    value 11
    latitude 36.1510984
  ]
  node [
    id 89
    label "NorthCarolina"
    longitude -79.0469134
    value 0
    latitude 35.9049122
  ]
  node [
    id 90
    label "UtahState"
    longitude -111.8097214
    value 5
    latitude 41.7451312
  ]
  node [
    id 91
    label "Army"
    longitude -73.9630948
    value 4
    latitude 41.3799259
  ]
  node [
    id 92
    label "Cincinnati"
    longitude -84.5149504
    value 4
    latitude 39.1329219
  ]
  node [
    id 93
    label "AirForce"
    longitude -104.8605556
    value 7
    latitude 38.9905556
  ]
  node [
    id 94
    label "Rutgers"
    longitude -74.1742202
    value 1
    latitude 40.7413871
  ]
  node [
    id 95
    label "Georgia"
    longitude -83.3773221
    value 9
    latitude 33.9480053
  ]
  node [
    id 96
    label "LouisianaState"
    longitude -91.1800023
    value 9
    latitude 30.4132579
  ]
  node [
    id 97
    label "LouisianaLafayette"
    longitude -92.0181642
    value 10
    latitude 30.2165443
  ]
  node [
    id 98
    label "Texas"
    longitude -97.7327641
    value 3
    latitude 30.28065429999999
  ]
  node [
    id 99
    label "Marshall"
    longitude -82.4292749
    value 6
    latitude 38.4231429
  ]
  node [
    id 100
    label "MichiganState"
    longitude -84.4791506
    value 2
    latitude 42.7041949
  ]
  node [
    id 101
    label "MiamiFlorida"
    longitude -80.2792843
    value 1
    latitude 25.7216374
  ]
  node [
    id 102
    label "Missouri"
    longitude -92.3277375
    value 3
    latitude 38.9403808
  ]
  node [
    id 103
    label "Clemson"
    longitude -82.8370896
    value 0
    latitude 34.6670459
  ]
  node [
    id 104
    label "NevadaLasVegas"
    longitude -115.1387236
    value 7
    latitude 36.1054711
  ]
  node [
    id 105
    label "WakeForest"
    longitude -80.2783004
    value 0
    latitude 36.1357102
  ]
  node [
    id 106
    label "Indiana"
    longitude -86.5149053
    value 2
    latitude 39.1691355
  ]
  node [
    id 107
    label "OklahomaState"
    longitude -97.0737222
    value 3
    latitude 36.1270236
  ]
  node [
    id 108
    label "OregonState"
    longitude -123.2794752
    value 8
    latitude 44.5637355
  ]
  node [
    id 109
    label "Maryland"
    longitude -77.0117719
    value 0
    latitude 38.8439864
  ]
  node [
    id 110
    label "TexasChristian"
    longitude -97.3637239
    value 4
    latitude 32.7078459
  ]
  node [
    id 111
    label "California"
    longitude -122.2505629
    value 8
    latitude 37.8739132
  ]
  node [
    id 112
    label "AlabamaBirmingham"
    longitude -86.8095148
    value 4
    latitude 33.4964621
  ]
  node [
    id 113
    label "Arkansas"
    longitude -94.1736551
    value 9
    latitude 36.0678324
  ]
  node [
    id 114
    label "Hawaii"
    longitude -157.86
    value 11
    latitude 21.31
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 38
  ]
  edge [
    source 12
    target 43
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 64
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 39
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 38
  ]
  edge [
    source 14
    target 71
  ]
  edge [
    source 14
    target 43
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 54
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 15
    target 32
  ]
  edge [
    source 15
    target 68
  ]
  edge [
    source 15
    target 39
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 47
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 60
  ]
  edge [
    source 16
    target 67
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 41
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 65
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 17
    target 27
  ]
  edge [
    source 17
    target 62
  ]
  edge [
    source 17
    target 95
  ]
  edge [
    source 18
    target 34
  ]
  edge [
    source 18
    target 99
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 38
  ]
  edge [
    source 18
    target 71
  ]
  edge [
    source 18
    target 42
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 18
    target 61
  ]
  edge [
    source 18
    target 31
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 36
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 44
  ]
  edge [
    source 19
    target 79
  ]
  edge [
    source 19
    target 94
  ]
  edge [
    source 19
    target 55
  ]
  edge [
    source 19
    target 29
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 20
    target 96
  ]
  edge [
    source 20
    target 65
  ]
  edge [
    source 20
    target 36
  ]
  edge [
    source 20
    target 70
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 75
  ]
  edge [
    source 20
    target 76
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 87
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 68
  ]
  edge [
    source 21
    target 108
  ]
  edge [
    source 21
    target 77
  ]
  edge [
    source 21
    target 46
  ]
  edge [
    source 21
    target 111
  ]
  edge [
    source 21
    target 51
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 68
  ]
  edge [
    source 22
    target 108
  ]
  edge [
    source 22
    target 77
  ]
  edge [
    source 22
    target 78
  ]
  edge [
    source 22
    target 47
  ]
  edge [
    source 22
    target 51
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 104
  ]
  edge [
    source 23
    target 78
  ]
  edge [
    source 23
    target 111
  ]
  edge [
    source 23
    target 41
  ]
  edge [
    source 23
    target 90
  ]
  edge [
    source 23
    target 93
  ]
  edge [
    source 24
    target 66
  ]
  edge [
    source 24
    target 69
  ]
  edge [
    source 24
    target 110
  ]
  edge [
    source 24
    target 50
  ]
  edge [
    source 24
    target 84
  ]
  edge [
    source 24
    target 87
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 90
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 25
    target 37
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 105
  ]
  edge [
    source 25
    target 106
  ]
  edge [
    source 25
    target 103
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 25
    target 109
  ]
  edge [
    source 25
    target 53
  ]
  edge [
    source 25
    target 89
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 38
  ]
  edge [
    source 26
    target 42
  ]
  edge [
    source 26
    target 43
  ]
  edge [
    source 26
    target 85
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 61
  ]
  edge [
    source 27
    target 96
  ]
  edge [
    source 27
    target 70
  ]
  edge [
    source 27
    target 65
  ]
  edge [
    source 27
    target 76
  ]
  edge [
    source 27
    target 56
  ]
  edge [
    source 27
    target 95
  ]
  edge [
    source 27
    target 62
  ]
  edge [
    source 27
    target 63
  ]
  edge [
    source 28
    target 69
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 78
  ]
  edge [
    source 28
    target 113
  ]
  edge [
    source 28
    target 50
  ]
  edge [
    source 28
    target 90
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 101
  ]
  edge [
    source 29
    target 42
  ]
  edge [
    source 29
    target 79
  ]
  edge [
    source 29
    target 80
  ]
  edge [
    source 29
    target 82
  ]
  edge [
    source 29
    target 94
  ]
  edge [
    source 29
    target 55
  ]
  edge [
    source 29
    target 91
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 101
  ]
  edge [
    source 30
    target 44
  ]
  edge [
    source 30
    target 82
  ]
  edge [
    source 30
    target 79
  ]
  edge [
    source 30
    target 50
  ]
  edge [
    source 30
    target 55
  ]
  edge [
    source 30
    target 109
  ]
  edge [
    source 30
    target 94
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 99
  ]
  edge [
    source 31
    target 71
  ]
  edge [
    source 31
    target 43
  ]
  edge [
    source 31
    target 79
  ]
  edge [
    source 31
    target 85
  ]
  edge [
    source 31
    target 54
  ]
  edge [
    source 31
    target 55
  ]
  edge [
    source 31
    target 61
  ]
  edge [
    source 32
    target 64
  ]
  edge [
    source 32
    target 100
  ]
  edge [
    source 32
    target 39
  ]
  edge [
    source 32
    target 106
  ]
  edge [
    source 32
    target 49
  ]
  edge [
    source 32
    target 47
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 103
  ]
  edge [
    source 33
    target 105
  ]
  edge [
    source 33
    target 45
  ]
  edge [
    source 33
    target 109
  ]
  edge [
    source 33
    target 89
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 71
  ]
  edge [
    source 34
    target 42
  ]
  edge [
    source 34
    target 99
  ]
  edge [
    source 34
    target 54
  ]
  edge [
    source 34
    target 61
  ]
  edge [
    source 34
    target 94
  ]
  edge [
    source 35
    target 101
  ]
  edge [
    source 35
    target 44
  ]
  edge [
    source 35
    target 79
  ]
  edge [
    source 35
    target 94
  ]
  edge [
    source 35
    target 55
  ]
  edge [
    source 35
    target 92
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 43
  ]
  edge [
    source 36
    target 58
  ]
  edge [
    source 36
    target 59
  ]
  edge [
    source 37
    target 105
  ]
  edge [
    source 37
    target 103
  ]
  edge [
    source 37
    target 45
  ]
  edge [
    source 37
    target 109
  ]
  edge [
    source 37
    target 80
  ]
  edge [
    source 37
    target 89
  ]
  edge [
    source 37
    target 95
  ]
  edge [
    source 38
    target 71
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 43
  ]
  edge [
    source 38
    target 85
  ]
  edge [
    source 38
    target 54
  ]
  edge [
    source 39
    target 100
  ]
  edge [
    source 39
    target 106
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 39
    target 82
  ]
  edge [
    source 39
    target 54
  ]
  edge [
    source 39
    target 60
  ]
  edge [
    source 40
    target 98
  ]
  edge [
    source 40
    target 102
  ]
  edge [
    source 40
    target 72
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 74
  ]
  edge [
    source 40
    target 107
  ]
  edge [
    source 40
    target 81
  ]
  edge [
    source 40
    target 51
  ]
  edge [
    source 40
    target 52
  ]
  edge [
    source 41
    target 67
  ]
  edge [
    source 41
    target 104
  ]
  edge [
    source 41
    target 93
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 57
  ]
  edge [
    source 42
    target 63
  ]
  edge [
    source 43
    target 70
  ]
  edge [
    source 43
    target 79
  ]
  edge [
    source 43
    target 85
  ]
  edge [
    source 43
    target 61
  ]
  edge [
    source 44
    target 112
  ]
  edge [
    source 44
    target 66
  ]
  edge [
    source 44
    target 75
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 86
  ]
  edge [
    source 44
    target 57
  ]
  edge [
    source 44
    target 91
  ]
  edge [
    source 45
    target 105
  ]
  edge [
    source 45
    target 103
  ]
  edge [
    source 45
    target 109
  ]
  edge [
    source 45
    target 89
  ]
  edge [
    source 45
    target 62
  ]
  edge [
    source 46
    target 67
  ]
  edge [
    source 46
    target 73
  ]
  edge [
    source 46
    target 110
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 114
  ]
  edge [
    source 46
    target 83
  ]
  edge [
    source 46
    target 53
  ]
  edge [
    source 46
    target 88
  ]
  edge [
    source 46
    target 111
  ]
  edge [
    source 47
    target 64
  ]
  edge [
    source 47
    target 100
  ]
  edge [
    source 47
    target 60
  ]
  edge [
    source 47
    target 61
  ]
  edge [
    source 48
    target 96
  ]
  edge [
    source 48
    target 66
  ]
  edge [
    source 48
    target 75
  ]
  edge [
    source 48
    target 98
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 53
  ]
  edge [
    source 48
    target 86
  ]
  edge [
    source 48
    target 57
  ]
  edge [
    source 48
    target 91
  ]
  edge [
    source 48
    target 92
  ]
  edge [
    source 49
    target 67
  ]
  edge [
    source 49
    target 73
  ]
  edge [
    source 49
    target 110
  ]
  edge [
    source 49
    target 114
  ]
  edge [
    source 49
    target 83
  ]
  edge [
    source 49
    target 84
  ]
  edge [
    source 49
    target 53
  ]
  edge [
    source 49
    target 88
  ]
  edge [
    source 50
    target 68
  ]
  edge [
    source 50
    target 69
  ]
  edge [
    source 50
    target 78
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 90
  ]
  edge [
    source 51
    target 68
  ]
  edge [
    source 51
    target 101
  ]
  edge [
    source 51
    target 108
  ]
  edge [
    source 51
    target 77
  ]
  edge [
    source 51
    target 78
  ]
  edge [
    source 51
    target 111
  ]
  edge [
    source 52
    target 98
  ]
  edge [
    source 52
    target 102
  ]
  edge [
    source 52
    target 74
  ]
  edge [
    source 52
    target 112
  ]
  edge [
    source 52
    target 72
  ]
  edge [
    source 52
    target 84
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 67
  ]
  edge [
    source 53
    target 73
  ]
  edge [
    source 53
    target 114
  ]
  edge [
    source 53
    target 83
  ]
  edge [
    source 53
    target 110
  ]
  edge [
    source 53
    target 86
  ]
  edge [
    source 53
    target 88
  ]
  edge [
    source 54
    target 99
  ]
  edge [
    source 54
    target 71
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 61
  ]
  edge [
    source 55
    target 101
  ]
  edge [
    source 55
    target 79
  ]
  edge [
    source 55
    target 94
  ]
  edge [
    source 55
    target 89
  ]
  edge [
    source 56
    target 96
  ]
  edge [
    source 56
    target 65
  ]
  edge [
    source 56
    target 70
  ]
  edge [
    source 56
    target 106
  ]
  edge [
    source 56
    target 76
  ]
  edge [
    source 56
    target 87
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 62
  ]
  edge [
    source 56
    target 95
  ]
  edge [
    source 57
    target 112
  ]
  edge [
    source 57
    target 75
  ]
  edge [
    source 57
    target 86
  ]
  edge [
    source 57
    target 91
  ]
  edge [
    source 57
    target 92
  ]
  edge [
    source 58
    target 97
  ]
  edge [
    source 58
    target 101
  ]
  edge [
    source 58
    target 114
  ]
  edge [
    source 58
    target 88
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 63
  ]
  edge [
    source 59
    target 97
  ]
  edge [
    source 59
    target 66
  ]
  edge [
    source 59
    target 76
  ]
  edge [
    source 59
    target 113
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 63
  ]
  edge [
    source 60
    target 64
  ]
  edge [
    source 60
    target 71
  ]
  edge [
    source 60
    target 106
  ]
  edge [
    source 61
    target 99
  ]
  edge [
    source 61
    target 71
  ]
  edge [
    source 61
    target 92
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 62
    target 70
  ]
  edge [
    source 62
    target 105
  ]
  edge [
    source 62
    target 76
  ]
  edge [
    source 62
    target 87
  ]
  edge [
    source 62
    target 95
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 63
    target 97
  ]
  edge [
    source 63
    target 109
  ]
  edge [
    source 63
    target 112
  ]
  edge [
    source 64
    target 100
  ]
  edge [
    source 64
    target 106
  ]
  edge [
    source 64
    target 111
  ]
  edge [
    source 65
    target 96
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 70
  ]
  edge [
    source 65
    target 113
  ]
  edge [
    source 65
    target 87
  ]
  edge [
    source 66
    target 91
  ]
  edge [
    source 66
    target 76
  ]
  edge [
    source 66
    target 75
  ]
  edge [
    source 66
    target 112
  ]
  edge [
    source 66
    target 86
  ]
  edge [
    source 66
    target 92
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 104
  ]
  edge [
    source 67
    target 110
  ]
  edge [
    source 67
    target 114
  ]
  edge [
    source 67
    target 83
  ]
  edge [
    source 67
    target 73
  ]
  edge [
    source 67
    target 88
  ]
  edge [
    source 68
    target 108
  ]
  edge [
    source 68
    target 78
  ]
  edge [
    source 68
    target 111
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 88
  ]
  edge [
    source 69
    target 83
  ]
  edge [
    source 69
    target 90
  ]
  edge [
    source 69
    target 91
  ]
  edge [
    source 69
    target 95
  ]
  edge [
    source 70
    target 103
  ]
  edge [
    source 70
    target 76
  ]
  edge [
    source 70
    target 113
  ]
  edge [
    source 70
    target 95
  ]
  edge [
    source 71
    target 99
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 72
    target 102
  ]
  edge [
    source 72
    target 107
  ]
  edge [
    source 72
    target 81
  ]
  edge [
    source 72
    target 104
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 73
    target 110
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 77
  ]
  edge [
    source 73
    target 114
  ]
  edge [
    source 73
    target 83
  ]
  edge [
    source 73
    target 88
  ]
  edge [
    source 74
    target 102
  ]
  edge [
    source 74
    target 82
  ]
  edge [
    source 74
    target 84
  ]
  edge [
    source 75
    target 112
  ]
  edge [
    source 75
    target 107
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 86
  ]
  edge [
    source 75
    target 92
  ]
  edge [
    source 76
    target 96
  ]
  edge [
    source 76
    target 113
  ]
  edge [
    source 76
    target 95
  ]
  edge [
    source 77
    target 98
  ]
  edge [
    source 77
    target 108
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 111
  ]
  edge [
    source 77
    target 82
  ]
  edge [
    source 78
    target 108
  ]
  edge [
    source 78
    target 111
  ]
  edge [
    source 79
    target 101
  ]
  edge [
    source 79
    target 109
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 94
  ]
  edge [
    source 80
    target 105
  ]
  edge [
    source 80
    target 110
  ]
  edge [
    source 80
    target 93
  ]
  edge [
    source 80
    target 82
  ]
  edge [
    source 80
    target 85
  ]
  edge [
    source 80
    target 86
  ]
  edge [
    source 80
    target 91
  ]
  edge [
    source 80
    target 94
  ]
  edge [
    source 81
    target 98
  ]
  edge [
    source 81
    target 107
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 83
  ]
  edge [
    source 81
    target 84
  ]
  edge [
    source 82
    target 100
  ]
  edge [
    source 82
    target 93
  ]
  edge [
    source 82
    target 94
  ]
  edge [
    source 83
    target 110
  ]
  edge [
    source 83
    target 114
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 88
  ]
  edge [
    source 84
    target 98
  ]
  edge [
    source 84
    target 107
  ]
  edge [
    source 85
    target 99
  ]
  edge [
    source 86
    target 97
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 91
  ]
  edge [
    source 86
    target 92
  ]
  edge [
    source 87
    target 96
  ]
  edge [
    source 87
    target 113
  ]
  edge [
    source 87
    target 104
  ]
  edge [
    source 87
    target 95
  ]
  edge [
    source 88
    target 107
  ]
  edge [
    source 88
    target 110
  ]
  edge [
    source 88
    target 114
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 89
    target 99
  ]
  edge [
    source 89
    target 105
  ]
  edge [
    source 89
    target 103
  ]
  edge [
    source 89
    target 109
  ]
  edge [
    source 91
    target 112
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 92
    target 112
  ]
  edge [
    source 92
    target 106
  ]
  edge [
    source 93
    target 104
  ]
  edge [
    source 94
    target 101
  ]
  edge [
    source 95
    target 113
  ]
  edge [
    source 96
    target 113
  ]
  edge [
    source 96
    target 112
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 112
  ]
  edge [
    source 98
    target 102
  ]
  edge [
    source 98
    target 107
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 100
    target 102
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 107
  ]
  edge [
    source 103
    target 105
  ]
  edge [
    source 103
    target 109
  ]
  edge [
    source 104
    target 114
  ]
  edge [
    source 105
    target 109
  ]
  edge [
    source 108
    target 111
  ]
  edge [
    source 110
    target 114
  ]
]
