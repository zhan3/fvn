//
//  GPU.cpp
//  
//
//  Created by Zhen Han on 9/24/14.
//
//

#include "GPU.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <assert.h>

using namespace std;

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}


__global__ void Initialize(float *latent, float value, unsigned long LENGTH){

	// Initialize
	for(unsigned long i=0;i<LENGTH;i++){
		latent[i] = value;
	}
}
__global__ void Update(float *latent, float *dx, unsigned long LENGTH){

	// update the latent place matrix

	// location of the kernel
	unsigned long tid = threadIdx.x + blockIdx.x * blockDim.x;

	// remove redundent kernels
	if(tid < LENGTH){

        	latent[2*tid] += dx[2*tid];
        	latent[2*tid + 1] += dx[2*tid + 1];
    
    	}

}

__global__ void UpdateModelParameter(float *alpha, float *alpha_dx, float *alpha_array, unsigned long LENGTH, float lr, float moment)
{

	/* 
		*alpha_dx: the velocity of alpha
		*alpha_array: the intermidate alpha update value array
	*/


	// Update alpha
	float alpha_update = 0.0;
	for(unsigned long i=0;i<LENGTH;i++){
		alpha_update += alpha_array[i];
	}

	*alpha_dx += *alpha_dx * moment - lr * alpha_update/2.0;
	*alpha += *alpha_dx;
	
}

__device__ float Logistic(float x){
	
	// Logistic transformation
	return 1/(1+expf(-x));
}
__global__ void Gradient(unsigned long *col,unsigned long *row, float *latent, float *dx, float *alpha, float *alpha_array, float *beta, float *beta_array, float lr, float moment, float LAMBDA,float *loglike_matrix,unsigned long NUM_NODE)
{
	/* 
		*col: contingency matrix indices
		*row: contingency matrix indptrs
		*latent: latent location 
		*dx: velocity 
		*alpha: model parameter
		*alpha_array: array for alpha update
		*beta: model parameter
		*beta_array: array for beta update
		lr: learning rate
		moment: moment
		LAMBDA: A constant
		*loglike_matrix: array for summing loglikelihood
	*/
	
	
	// location of the kernel
	unsigned long tid = threadIdx.x + blockIdx.x * blockDim.x;
    
	// remove redundent kernels
	if(tid < NUM_NODE){
        
	float BETA = *beta;
	float ALPHA = *alpha;

        // derivative of the locaions dx_1 dx_2
        float dx_1 = 0.0;
        float dx_2 = 0.0;
        
        // parameter update intemediate variable 
	float alpha_update = 0.0;
	float beta_update = 0.0;

	// initialize the loglikelihood for node tid
        float loglike = 0.0;
        
	// extract connected nodes from col and row
  	unsigned long start = row[tid];
  	unsigned long end = row[tid + 1];
  	unsigned long num_connected = end - start;

	// Speed imporvement by not allocating memory 
	// unsigned long * connected_neig_array = (unsigned long *)malloc(sizeof(unsigned long)*num_connected);
	// Copy the neighbour array to register memory
	//int j = 0;
	//for(unsigned long i =row[tid];i<row[tid+1];i++ ){
	//	connected_neig_array[j++] = col[i];
	//}


        // iterate through each node in the network
        int j = 0;
	float loc_tid_1 = latent[2*tid];
	float loc_tid_2 = latent[2*tid + 1];
	unsigned long node_connected_next = col[start + j];

        for(unsigned long i = 0;i < NUM_NODE;i++){
        
		// ignore self connected
            	if (tid == i){continue;}
            
            	// some temperary variables
		float d1 = loc_tid_1 - latent[2*i];
		float d2 = loc_tid_2 - latent[2*i+1];
            	float distance = powf(d1,2) + powf(d2,2);
            	float Pij = Logistic(ALPHA + BETA*distance);
		Pij = Pij*0.9999 + 0.00005;
		float temp_1 = 2*BETA*d1;
            	float temp_2 = 2*BETA*d2;
	
            	// Connected neighbour node
		if (j < num_connected){
			if (i == node_connected_next ){
          	      		// do something for connected neighbour
				dx_1 += -temp_1*(1-Pij);
                    		dx_2 += -temp_2*(1-Pij);
                    		loglike += log(Pij);
				alpha_update += -(1-Pij);
				beta_update += 	-distance * (1-Pij);	

				j++;
				node_connected_next = col[start + j];
                    		continue;
                	}
            	}
            
            	// non-neighbhour node 
            	// do something for non-connected node
            	dx_1 += temp_1*Pij;
            	dx_2 += temp_2*Pij;
  	    	loglike += log(1 - Pij);
		alpha_update += Pij;
                beta_update +=  distance * Pij;
        }
	// Add the penalization term
	dx_1 += 2*LAMBDA*loc_tid_1;
	dx_2 += 2*LAMBDA*loc_tid_2;
        
        // printf("%f \n",dx_1);
	// store the update to the global array
        dx[2*tid] = moment * dx[2*tid] - lr * dx_1;
        dx[2*tid+1] = moment * dx[2*tid+1] - lr * dx_2;

	// Store the alpha beta update 
	alpha_array[tid] = alpha_update;
	beta_array[tid] = beta_update;

	// store the loglike for one node in the queue
        loglike_matrix[tid] = loglike;

	// free up the on device memory
	// free(connected_neig_array);
        
	}
    
}

void ReadFile(FILE *fr,unsigned long *data_array)
{
	// Read file into data_array
	char line[80];
	unsigned long i = 0;

	while(fgets(line, 80, fr) != NULL)
    	{
        	sscanf (line,"%lu",&data_array[i]);
        	i++;
   	}

}
unsigned long FileLength(char *filename){

	FILE *fr = fopen (filename, "rt");
	char ch=0;
	unsigned long lines=0;

	while(!feof(fr))
	{
  		ch = fgetc(fr);
  		if(ch == '\n')
  		{
    			lines++;
  		}
	}	
	lines++;
	return lines;
}

float GenerateRandomNumber()
{
	// random number generator between 0 and 1
   	return (float)rand() / (float)RAND_MAX ;
}

void FillRandom(float *data_array, unsigned long LENGTH)
{
	for(unsigned long i=0; i<LENGTH; i++){
		data_array[i] = GenerateRandomNumber() - 0.5;
	}
}

int main(int argc, char *argv[])
{
	srand(time(NULL));

	if ( argc != 11 ) 
   	{
    		printf( "usage: indices,indptr,lr,moment1,moment2,NumIter,lr_decay,lambda,alpha0,beta0");
		return 1;
	}
	const unsigned long COL_LEN = FileLength(argv[1]);
	const unsigned long ROW_LEN = FileLength(argv[2]);
	const unsigned long NUM_NODE = ROW_LEN - 1;
	int blocksize = 512;
	/*if(NUM_NODE < 512){
		blocksize = 512;
	}*/
	int nblocks = (NUM_NODE + blocksize - 1)/blocksize;
	printf("Number of nodes: %lu \n",NUM_NODE);
	printf("Block count: %d, Blocksize %d \n",nblocks,blocksize);
        float lr = atof(argv[3]);
       	float moment_1 = atof(argv[4]);
        float moment_2 = atof(argv[5]);
        int NUM_ITERATION = atoi(argv[6]);
        int LR_Decay = atoi(argv[7]);
        float LAMBDA = atof(argv[8]);
        float alpha_initial = atof(argv[9]);
        float beta_initial = atof(argv[10]);
    	
	printf("Parameter setup \n");
	printf("lr: %f, moment_1: %f, moment_2: %f, NumIter: %d, lr_decay: %d, lambda: %f,  alpha0: %f, beta0: %f \n",lr,moment_1,moment_2,NUM_ITERATION,LR_Decay,LAMBDA,alpha_initial,beta_initial);

	// declare a pointer variable to point to allocated heap space
    	unsigned long *col_array;
    	unsigned long *row_array;
    	float *latent_space_matrix;
    	float *loglike_matrix;
    
	// call malloc to allocate that appropriate number of bytes for the array
        col_array = (unsigned long *)malloc(sizeof(unsigned long)*COL_LEN);
        row_array = (unsigned long *)malloc(sizeof(unsigned long)*ROW_LEN);
        latent_space_matrix = (float *)malloc(sizeof(float)*NUM_NODE*2);
        loglike_matrix = (float *)malloc(sizeof(float)*NUM_NODE);

    	// GPU variables
	unsigned long *dev_col_array;
    	unsigned long *dev_row_array;    
    	float *dev_latent_space_matrix;
    	float *dev_dx_matrix;
    	float *dev_loglike_matrix;
	float *alpha;
	float *alpha_array;
	float *alpha_dx;
	float *beta;
	float *beta_array;
	float *beta_dx;

    	// allocate space on GPU device
    	gpuErrchk(cudaMalloc((void**)&dev_col_array, COL_LEN*sizeof(unsigned long)));
    	gpuErrchk(cudaMalloc((void**)&dev_row_array, ROW_LEN*sizeof(unsigned long)));
    	gpuErrchk(cudaMalloc((void**)&dev_latent_space_matrix, NUM_NODE*2*sizeof(float)));
    	gpuErrchk(cudaMalloc((void**)&dev_dx_matrix, NUM_NODE*2*sizeof(float))); 
    	gpuErrchk(cudaMalloc((void**)&dev_loglike_matrix, NUM_NODE*sizeof(float))); 
	gpuErrchk(cudaMalloc((void**)&alpha, sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&alpha_dx, sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&alpha_array, NUM_NODE*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&beta, sizeof(float)));
        gpuErrchk(cudaMalloc((void**)&beta_dx, sizeof(float)));
        gpuErrchk(cudaMalloc((void**)&beta_array, NUM_NODE*sizeof(float)));

    	// construct the col_array from text file
    	FILE *fr = fopen (argv[1], "rt");
	ReadFile(fr,col_array);    
    	fclose(fr);
    	// construct the row from text file
   	fr = fopen (argv[2], "rt");
    	ReadFile(fr,row_array);
	fclose(fr);
	// construct the latent space matrix from random 0,1
	FillRandom(latent_space_matrix,NUM_NODE*2);
	
    	// copy data over to the device
    	gpuErrchk(cudaMemcpy(dev_col_array,col_array,COL_LEN*sizeof(unsigned long),cudaMemcpyHostToDevice));
    	gpuErrchk(cudaMemcpy(dev_row_array,row_array,ROW_LEN*sizeof(unsigned long),cudaMemcpyHostToDevice));
    	gpuErrchk(cudaMemcpy(dev_latent_space_matrix,latent_space_matrix,NUM_NODE*2*sizeof(float),cudaMemcpyHostToDevice));

    	// printf("Launching Kernel \n");

    	// set initial dx to be 0
	Initialize<<<1,1>>>(dev_dx_matrix, 0, NUM_NODE*2);
	// set initial loglike to be 0
    	Initialize<<<1,1>>>(dev_loglike_matrix, 0, NUM_NODE);
	// Set initial parameter update intermediate values
	Initialize<<<1,1>>>(alpha_dx, 0, 1);
	Initialize<<<1,1>>>(alpha_array, 0, NUM_NODE);
	Initialize<<<1,1>>>(beta_dx, 0, 1);
	Initialize<<<1,1>>>(beta_array, 0, NUM_NODE);

	// Set initial alpha and beta
	Initialize<<<1,1>>>(alpha, alpha_initial, 1);
	Initialize<<<1,1>>>(beta, beta_initial, 1);
	
	// error checking
	gpuErrchk( cudaPeekAtLastError() );

	// timing
	clock_t t;
	t = clock();

    	//  Iterate
	float moment = moment_1;
    	for(int iter = 0; iter < NUM_ITERATION; iter++ ){
        
		if(iter > 10){
			moment = moment_2;
		}
        	// Calculate gradient
    		Gradient<<<nblocks,blocksize>>>(dev_col_array,dev_row_array,dev_latent_space_matrix,dev_dx_matrix,alpha,alpha_array,beta,beta_array,lr,moment,LAMBDA,dev_loglike_matrix,NUM_NODE);
		
		gpuErrchk( cudaPeekAtLastError() );

		// Update 
        	Update<<<nblocks,blocksize>>>(dev_latent_space_matrix,dev_dx_matrix,NUM_NODE);        
	
		gpuErrchk( cudaPeekAtLastError() );

		// Update model parameter
		UpdateModelParameter<<<1,1>>>(alpha, alpha_dx, alpha_array, NUM_NODE, lr, 0);
		UpdateModelParameter<<<1,1>>>(beta, beta_dx, beta_array, NUM_NODE, lr, 0);
		
		gpuErrchk( cudaPeekAtLastError() );

		// A CPU STEP to sum up the loglikelihood
		if(iter%100 ==0){
        		cudaMemcpy(loglike_matrix,dev_loglike_matrix,NUM_NODE*sizeof(float),cudaMemcpyDeviceToHost);
        		float LogLike = 0;
        		for(unsigned long iter_2 = 0; iter_2 < NUM_NODE; iter_2++){
            			LogLike += loglike_matrix[iter_2];
        		}
        		printf("Log likelihood at itaration %lu %f \n",iter,LogLike);
		}

        	// synchronize
        	//cudaDeviceSynchronize();
		gpuErrchk( cudaDeviceSynchronize() );
        
        	//dampe the learning rate
        	if((iter+1)%LR_Decay == 0){
            		lr*= 0.95;
        	}
    	}

	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC;
	printf("Kernel finished in %f seconds\n", time_taken);

	// copy data back to the host
    	gpuErrchk(cudaMemcpy(latent_space_matrix,dev_latent_space_matrix,NUM_NODE*2*sizeof(float),cudaMemcpyDeviceToHost));
    	
	float alpha_fitted = 0.0;
	float beta_fitted = 0.0;
	gpuErrchk(cudaMemcpy(&alpha_fitted,alpha,sizeof(float),cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(&beta_fitted,beta,sizeof(float),cudaMemcpyDeviceToHost));
    	
	printf("Dumping latent space to disk \n");
    
    	// Before free up the memory dump the latent space matrix to disk and save for examation
    	ofstream ofs ("latent_space_matrix.csv");  
    	if (ofs.is_open()){
        	for (unsigned long t =0;t< NUM_NODE;t++){
            		ofs << latent_space_matrix[2*t]<<","<<latent_space_matrix[2*t+1]<<endl;
        	}	
        	ofs.close();
    	}
    	else printf("Unable to open file \n");
	    
	// Wrtie to txt file for alpha and beta
	FILE *f = fopen("Model_parameter.txt", "w");
	if (f == NULL)
	{
    		printf("Error opening file!\n");
    		exit(1);
	}
	else{
		fprintf(f, "alpha: %f\n", alpha_fitted);
		fprintf(f, "beta: %f\n", beta_fitted);
	}

    	// free up the memory on device and host
    	cudaFree(dev_col_array);
    	cudaFree(dev_row_array);
    	cudaFree(dev_latent_space_matrix);
    	cudaFree(dev_dx_matrix);
    	cudaFree(dev_loglike_matrix);
    	cudaFree(alpha);
	cudaFree(alpha_dx);
	cudaFree(alpha_array);
	cudaFree(beta);
	cudaFree(beta_dx);
	cudaFree(beta_array);

    	free(col_array);
    	free(row_array);
    	free(latent_space_matrix);
    	free(loglike_matrix);

    	printf("Finshed \n");
}
